@extends('admin.master')
@section('content-heading')
Income Type
@endsection

@section('main-content')
<div class="panel-body">
	<div class="row">
		<div class="col-lg-6">
			<form role="form">
				{!! Form::open(['method' => 'POST', 'url' => '/incomes', 'role'=>'form']) !!}
				<div class="form-group">
					{!! Form::label('name','Income Name:') !!}
					{!! Form::text('name', isset($income->name) ? $income->name : null, ['class'=> 'form-control']) !!}
				</div>
				<div class="form-group">
					<div class="text-right">
						{!! Form::submit('Submit', array('class'=> 'btn btn-success')) !!}
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	@endsection